# Docker web system 
## 1 - Use Docker CLI
- Source code : wpcode
- Instructions file : lab-docker-cli.md
## 2 - Use Dockerfile & docker-compose.yaml
- Source code : newwpcode
- Instructions file : lab-docker-compose.md
PHP:7.3-fpm (php-product)
    - port: 9000
    - cài mysqli, pdo_mysql:
        + docker-php-ext-install mysqli
        + docker-php-ext-install pdo_mysql
    - thư mục làm việc: /home/newwpcode/www

Apache httpd (c-httpd)
    - port: 80, 443
    - config: /usr/local/apache2/conf/httpd.conf
        + nạp mod_proxy, mod_proxy_fcgi
        + thư mục làm việc: /home/newwpcode/www
        + index mặc định: index.html index.php
        + PHPHANDELER: AddHandler "proxy:fcgi://php-product:9000"

MySQL: (mysql-product)
    - port: 3306
    - config: /etc/my.cnf
        + default-authentication-plugin=mysql_native_password
    - databases: /var/lib/mysql -> /home/tangnd/newwpcode/db
    - MYSQL_ROOT_PASSWORD: 123abc
    - MYSQL_DATABASE: dbsite
    - MYSQL_USER: siteuser
    - MYSQL_PASSWORD: sitepass

Network:
    - bridge: 
        + wp-network

Volume : dir-site
    - bind, devide = /home/tangnd/newwpcode/

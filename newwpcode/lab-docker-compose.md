# Setup Web Service (Wordpress) by Docker file & Docker compose

## 1 - Write Dockerfile for php (./php/Dockerfile)

```bash
    FROM php:7.3-fpm

    RUN docker-php-ext-install mysqli
    RUN docker-php-ext-install pdo_mysql

    WORKDIR /home/newwpcode/www
```
## 2 - Config httpd.conf
a - Move file httpd.conf from container
```
   $ docker run --rm -v /home/username/wpcode/:/home/wpcode/ httpd cp /usr/local/apache2/conf/httpd.conf /home/wpcode/
```
b - Edit file httpd.conf on host machine
- remove comment 2 row 
    ```bash
        LoadModule proxy_module modules/mod_proxy.so
        LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so
    ```
    ![alt text](images/1.png)

- add row to the end of the file 
    ```bash
        AddHandler "proxy:fcgi://wp-php:9000" .php
    ```
    ![alt text](images/2.png)

- Add folder default works (DocumentRoot)
    ![alt text](images/3.png)

- Add file index.php
    ![alt text](images/4.png)

c - Run container httpd
```
    $ docker run --network www-net --name wp-httpd -p 9999:80 -p 443:443 -h wp-httpd -v /home/username/wpcode/:/home/wpcode/ -v /home/username/wpcode/httpd.conf:/usr/local/apache2/conf/httpd.conf httpd
```

## 3 - Setup and Configure MySQL
a - Move file my.cnf from container
```
    $ docker run --rm -v /home/username/wpcode/:/home/wpcode/ mysql cp /etc/my.cnf /home/wpcode/
```
b - Edit file my.cnf on host machine
remove comment
```bash
    default-authentication-plugin=mysql_native_password
```
![alt text](images/5.png)

c - Create folder store database
```
    $ mkdir /home/username/wpcode/db
```

## 4 - Write docker-compose.yaml
```bash
    version: "3"

    #Network
    networks:
    wp-net:
    driver: bridge


    #Volumes
    volumes:
    dir-site:
    driver: local
    driver_opts:
        device: /home/username/newwpcode/
        o: bind
        type: none


    #Containers
    services:
    #php
    wp-php:
    container_name: php-product
    build:
        dockerfile: Dockerfile
        context: ./php/
    hostname: php
    restart: always
    networks:
        - wp-net
    volumes:
        - dir-site:/home/newwpcode/


    #httpd
    wp-httpd:
    container_name: c-httpd
    image: "httpd:latest"
    hostname: httpd
    restart: always
    networks:
        - wp-net
    volumes:
        - dir-site:/home/newwpcode/
        - ./httpd.conf:/usr/local/apache2/conf/httpd.conf
    ports:
        - "9999:80"
        - "443:443"


    #mysql
    wp-mysql:
    container_name: mysql-product
    image: "mysql:latest"
    hostname: mysql
    restart: always
    networks:
        - wp-net
    volumes:
        - ./db:/var/lib/mysql
        - ./my.cnf:/etc/my.cnf
    environment:
        - MYSQL_ROOT_PASSWORD=abc123
        - MYSQL_DATABASE=wp_db
        - MYSQL_USER=wpuser
        - MYSQL_PASSWORD=wppass
```

## 5 - Run service
```
    $ docker compose up
```
- access localhost:9999 and follow the instructions


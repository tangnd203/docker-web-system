<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_db' );

/** Database username */
define( 'DB_USER', 'wpuser' );

/** Database password */
define( 'DB_PASSWORD', 'wppass' );

/** Database hostname */
define( 'DB_HOST', 'wp-mysql' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`9a*?3=3YPH2wPT+SCSI8*7O/V9w/]HcTf!3$XyoM7v*|hq!LF`Jsk5^/.DU }uv' );
define( 'SECURE_AUTH_KEY',  'uX=]`P#2v=W9*]E/g~O9QG.fH?<bD=wICINWg9 &OADy~~na>xn4YwatCiXTiNGA' );
define( 'LOGGED_IN_KEY',    't8W];o_5Av:C4*,Xpm~~VC.w cg)<yIJ=x>{RG_US(/==1J 28-=gb|gB/0fEfqp' );
define( 'NONCE_KEY',        'cpRyI&s,Fo4uBkZEDDk:7Lg/> u@o9YI}Yfd>>cT;(&E)bX0lq~uXJ#Yf8_Uj=U)' );
define( 'AUTH_SALT',        'xWR+x7tOSg26nh)*S*IE~TL<cvAf%sa@{EP5_StdQqT[i!?/Lv~kf:&S>,Nf>RrJ' );
define( 'SECURE_AUTH_SALT', 'RX^ki1JNNE4[hqM( s6F`zDB<Cv,8GiJlnfvq(Zo:D8B|hBjBl;nu2UOo=yqhfM!' );
define( 'LOGGED_IN_SALT',   '.S6O3XmziCMZ(rIZCBO)$z!8-wk9Mi!6L?%-PYC*x -fJT2mc:IMIC[scsY/AU<N' );
define( 'NONCE_SALT',       'sJ;|=*?7MytE9$!fr;@ba`r_)?^<t$c!GB~iw9mvV_-`,#dp{u/,Qot=epLKhj&q' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

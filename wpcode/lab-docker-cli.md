# Setup Web Service (Wordpress) on Docker CLI
## 1 - Pull images from Docker hub
```
    $ docker pull httpd
    $ docker pull php:7.3-fpm
    $ docker pull mysql
```
## 2 - Create network bridge
```
    $ docker network create --driver bridge www-net
```
## 3 - Run containner PHP
a - Create folder contain code on host machine
  ```
    $ mkdir /home/username/wpcode
    $ mkdir ~/wpcode/www
  ```
b - Run container php
  ```
    $ docker run -d --name wp-php -h wp-php -v /home/username/wpcode/:/home/wpcode/ --network www-net php:7.3-fpm
  ```
## 4 - Configure webserver httpd
a - Move file httpd.conf from container
```
   $ docker run --rm -v /home/username/wpcode/:/home/wpcode/ httpd cp /usr/local/apache2/conf/httpd.conf /home/wpcode/
```
b - Edit file httpd.conf on host machine
- remove comment 2 row 
    ```bash
        LoadModule proxy_module modules/mod_proxy.so
        LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so
    ```
    ![alt text](images/1.png)

- add row to the end of the file 
    ```bash
        AddHandler "proxy:fcgi://wp-php:9000" .php
    ```
    ![alt text](images/2.png)

- Add folder default works (DocumentRoot)
    ![alt text](images/3.png)

- Add file index.php
    ![alt text](images/4.png)

c - Run container httpd
```
    $ docker run --network www-net --name wp-httpd -p 9999:80 -p 443:443 -h wp-httpd -v /home/username/wpcode/:/home/wpcode/ -v /home/username/wpcode/httpd.conf:/usr/local/apache2/conf/httpd.conf httpd
```
## 5 - Setup and Configure MySQL
a - Move file my.cnf from container
```
    $ docker run --rm -v /home/username/wpcode/:/home/wpcode/ mysql cp /etc/my.cnf /home/wpcode/
```
b - Edit file my.cnf on host machine
remove comment
```bash
    default-authentication-plugin=mysql_native_password
```
![alt text](images/5.png)

c - Create folder store database
```
    $ mkdir /home/username/wpcode/db
```
d - Run container MySQL
```
    $ docker run -e MYSQL_ROOT_PASSWORD=abc123 -v /home/username/wpcode/my.cnf:/etc/my.cnf -v /home/username/wpcode/db/:/var/lib/mysql --name wp-mysql -h wp-mysql --network www-net mysql
```
e - Create user, database, grant user permission to use the databases
```
    $ mysql -u root -p
    > create database wp_db;
    > CREATE USER 'wpuser'@'%' IDENTIFIED BY 'wppass';
    > GRANT ALL PRIVILEGE ON wp_db.* TO 'wpuser'@'%';
    > FLUSH PRIVILEGES;
```

## 6 - Setup wordpress
- install wordpress
- copy source code from wordpress to wpcode/www
- install extension mysqli and pdo_mysql for php
```
    $ docker exec -it wp-php bash
    $ docker-php-ext-install mysqli 
    $ docker-php-ext-install pdo_mysql
    $ php -m
```
- access localhost:9999 and follow the instructions


